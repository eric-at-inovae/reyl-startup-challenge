<?php
$databases['default']['default'] = array(
  'database' => 'reyl_startup_challenge',
  'username' => 'reyl_startup_challenge',
  'password' => 'reyl_startup_challenge',
  'host' => 'localhost',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
);

$settings['hash_salt'] = 'reyl-startup-challenge.local';

$config_directories = [
  'sync' => '../config/sync'
];
