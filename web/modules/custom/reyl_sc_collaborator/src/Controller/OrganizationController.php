<?php

namespace Drupal\reyl_sc_collaborator\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\user\Controller\UserController;

/**
 * Class CollaboratorController.
 *
 * Returns responses for Collaborator routes.
 */
class CollaboratorController extends UserController implements ContainerInjectionInterface {

}
