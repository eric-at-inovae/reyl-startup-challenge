<?php

namespace Drupal\reyl_sc_collaborator;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\reyl_sc_collaborator\Entity\CollaboratorInterface;

/**
 * Defines a class to build a listing of Collaborator entities.
 *
 * @ingroup reyl_sc_collaborator
 */
class CollaboratorListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');

    return $header + parent::buildHeader();
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  public function buildRow(EntityInterface $entity) {
    /** @var CollaboratorInterface $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->getDisplayName(),
      'entity.collaborator.canonical',
      [
        'collaborator' => $entity->id(),
      ]
    );

    return $row + parent::buildRow($entity);
  }
}
