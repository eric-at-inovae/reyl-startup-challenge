<?php

namespace Drupal\reyl_sc_collaborator\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Collaborator entities.
 *
 * @ingroup reyl_sc_collaborator
 */
interface CollaboratorInterface extends UserInterface {

}
