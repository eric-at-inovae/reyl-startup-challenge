<?php

namespace Drupal\reyl_sc_collaborator\Entity;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Collaborator entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class CollaboratorRouteProvider extends AdminHtmlRouteProvider {

}

