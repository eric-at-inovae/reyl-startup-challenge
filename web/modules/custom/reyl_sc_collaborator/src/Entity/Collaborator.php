<?php

namespace Drupal\reyl_sc_collaborator\Entity;

use Drupal\user\Entity\User;

/**
 * Defines the Collaborator entity.
 *
 * @ingroup reyl_sc_collaborator
 *
 * @ContentEntityType(
 *   id = "collaborator",
 *   label = @Translation("Collaborator"),
 *   label_collection = @Translation("Collaborators"),
 *   label_singular = @Translation("collaborator"),
 *   label_plural = @Translation("collaborators"),
 *   label_count = @PluralTranslation(
 *     singular = "@count collaborator",
 *     plural = "@count collaborators",
 *   ),
 *   handlers = {
 *    "list_builder" = "Drupal\reyl_sc_collaborator\CollaboratorListBuilder",
 *    "route_provider" = {
 *       "html" = "Drupal\reyl_sc_collaborator\Entity\CollaboratorRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\reyl_sc_collaborator\Form\CollaboratorForm",
 *       "edit" = "Drupal\reyl_sc_collaborator\Form\CollaboratorForm"
 *     },
 *   },
 *   base_table = "collaborator",
 *   revision_table = "collaborator_revision",
 *   revision_data_table = "collaborator_field_revision",
 *   admin_permission = "administer collaborator entities",
 *   entity_keys = {
 *     "id" = "uid",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/collaborator/{collaborator}",
 *     "add-form" = "/collaborator/add",
 *     "edit-form" = "/admin/content/collaborator/{collaborator}/edit",
 *     "delete-form" = "/admin/content/collaborator/{collaborator}/delete",
 *     "version-history" =
 *   "/admin/content/collaborator/{collaborator}/revisions",
 *     "revision" =
 *   "/admin/content/collaborator/{collaborator}/revisions/{collaborator_revision}/view",
 *     "revision_revert" =
 *   "/admin/content/collaborator/{collaborator}/revisions/{collaborator_revision}/revert",
 *     "revision_delete" =
 *   "/admin/content/collaborator/{collaborator}/revisions/{collaborator_revision}/delete",
 *     "collection" = "/admin/content/collaborator",
 *   },
 *   field_ui_base_route = "entity.collaborator.collection"
 * )
 */
class Collaborator extends User implements CollaboratorInterface {

}
