<?php

namespace Drupal\reyl_sc_collaborator\Form;

use Drupal\user\ProfileForm;

/**
 * Form controller for Collaborator edit forms.
 *
 * @ingroup reyl_sc_collaborator
 */
class CollaboratorForm extends ProfileForm {

}
