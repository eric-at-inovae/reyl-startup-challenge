<?php

namespace Drupal\reyl_sc_collaborator;

use Drupal\user\UserStorageInterface;

/**
 * Defines the storage handler class for Collaborator entities.
 *
 * This extends the base storage class, adding required special handling for
 * Collaborator entities.
 *
 * @ingroup reyl_sc_collaborator
 */
interface CollaboratorStorageInterface extends UserStorageInterface {

}
