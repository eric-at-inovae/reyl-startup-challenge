<?php

namespace Drupal\Tests\reyl_sc_collaborator\Functional;

use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;

/**
 * Basic access tests for Collaborator.
 *
 * @group reyl_sc_collaborator
 */
class AccessTest extends FunctionalTestBase {

  use AssertPageCacheContextsAndTagsTrait;
}
