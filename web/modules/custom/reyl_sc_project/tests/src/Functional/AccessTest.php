<?php

namespace Drupal\Tests\reyl_sc_project\Functional;

use Drupal\reyl_sc_project\Entity;
use Drupal\Tests\reyl_sc_project\Functional\FunctionalTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;

/**
 * Basic access tests for Project.
 *
 * @group reyl_sc_project
 */
class AccessTest extends FunctionalTestBase {

  use AssertPageCacheContextsAndTagsTrait;

  /**
   * Test some access control functionality.
   */
  public function testProjectAccess() {
    $assert_session = $this->assertSession();

    // Create media.
    $org = Project::create([
      'name' => 'Unnamed',
    ]);

    $org->save();

    $user_org = Project::create([
      'name' => 'Unnamed',
      'uid' => $this->nonAdminUser->id(),
    ]);

    $user_org->save();

    // We are logged in as admin, so test 'administer project' permission.
    $this->drupalGet('project/add');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('project/' . $user_org->id());
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('project/' . $user_org->id() . '/edit');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('project/' . $user_org->id() . '/delete');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);

    $this->drupalLogin($this->nonAdminUser);
    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load(RoleInterface::AUTHENTICATED_ID);
  }
}
