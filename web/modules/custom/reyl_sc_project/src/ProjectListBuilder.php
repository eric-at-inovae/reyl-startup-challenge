<?php

namespace Drupal\reyl_sc_project;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Project entities.
 *
 * @ingroup reyl_sc_project
 */
class ProjectListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['owner_name'] = $this->t('Owner');
    $header['organization_name'] = $this->t('Organization');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\reyl_sc_project\Entity\Project */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.project.canonical',
      [
        'project' => $entity->id()
      ]
    );
    $row['owner_name'] = Link::createFromRoute(
      $entity->getOwner()->getDisplayName(),
      'entity.user.canonical',
      [
        'user' => $entity->getOwnerId()
      ]
    );
    $row['organization_name'] = Link::createFromRoute(
      $entity->getOrganization()->getName(),
      'entity.organization.canonical',
      [
        'organization' => $entity->getOrganization()->id()
      ]
    );

    return $row + parent::buildRow($entity);
  }
}
