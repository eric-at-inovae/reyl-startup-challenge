<?php

namespace Drupal\reyl_sc_project\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\reyl_sc_organization\Entity\Organization;
use Drupal\reyl_sc_organization\Entity\OrganizationInterface;

/**
 * Provides an interface for defining Project entities.
 *
 * @ingroup reyl_sc_project
 */
interface ProjectInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Project name.
   *
   * @return string
   *   Name of the Project.
   */
  public function getName();

  /**
   * Sets the Project name.
   *
   * @param string $name
   *   The Project name.
   *
   * @return \Drupal\reyl_sc_project\Entity\ProjectInterface
   *   The called Project entity.
   */
  public function setName($name);

  /**
   * Gets the Project creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Project.
   */
  public function getCreatedTime();

  /**
   * Sets the Project creation timestamp.
   *
   * @param int $timestamp
   *   The Project creation timestamp.
   *
   * @return \Drupal\reyl_sc_project\Entity\ProjectInterface
   *   The called Project entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Project enabled status indicator.
   *
   * @return bool
   *   TRUE if the Project is enabled.
   */
  public function isEnabled();

  /**
   * Sets the enabled status of a Project.
   *
   * @param bool $flag
   *   TRUE to set this Project to enabled, FALSE to set it to disabled.
   *
   * @return \Drupal\reyl_sc_project\Entity\ProjectInterface
   *   The called Project entity.
   */
  public function setEnabled($flag);

  /**
   * Gets the Project revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Project revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\reyl_sc_project\Entity\ProjectInterface
   *   The called Project entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Project revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Project revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\reyl_sc_project\Entity\ProjectInterface
   *   The called Project entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Gets the organization the project belongs to.
   *
   * @return OrganizationInterface The organization the project belongs to.
   */
  public function getOrganization();
}
