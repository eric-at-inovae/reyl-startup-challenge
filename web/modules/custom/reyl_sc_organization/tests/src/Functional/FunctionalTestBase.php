<?php

namespace Drupal\Tests\reyl_sc_organization\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base class for REYL Startup Challenge functional tests.
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use FunctionalTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'reyl_sc_organization',
  ];
}
