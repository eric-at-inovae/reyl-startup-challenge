<?php

namespace Drupal\Tests\reyl_sc_organization\Functional;

use Drupal\reyl_sc_organization\Entity;
use Drupal\Tests\reyl_sc_organization\Functional\FunctionalTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\Tests\system\Functional\Cache\AssertPageCacheContextsAndTagsTrait;

/**
 * Basic access tests for Organization.
 *
 * @group reyl_sc_organization
 */
class AccessTest extends FunctionalTestBase {

  use AssertPageCacheContextsAndTagsTrait;

  /**
   * Test some access control functionality.
   */
  public function testOrganizationAccess() {
    $assert_session = $this->assertSession();

    // Create media.
    $org = Organization::create([
      'name' => 'Unnamed',
    ]);

    $org->save();

    $user_org = Organization::create([
      'name' => 'Unnamed',
      'uid' => $this->nonAdminUser->id(),
    ]);

    $user_org->save();

    // We are logged in as admin, so test 'administer organization' permission.
    $this->drupalGet('organization/add');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('organization/' . $user_org->id());
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('organization/' . $user_org->id() . '/edit');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);
    $this->drupalGet('organization/' . $user_org->id() . '/delete');
    $this->assertCacheContext('user.permissions');
    $assert_session->statusCodeEquals(200);

    $this->drupalLogin($this->nonAdminUser);
    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load(RoleInterface::AUTHENTICATED_ID);
  }
}
