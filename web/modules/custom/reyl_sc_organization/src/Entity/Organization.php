<?php

namespace Drupal\reyl_sc_organization\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Organization entity.
 *
 * @ingroup reyl_sc_organization
 *
 * @ContentEntityType(
 *   id = "organization",
 *   label = @Translation("Organization"),
 *   label_collection = @Translation("Organizations"),
 *   label_singular = @Translation("organization"),
 *   label_plural = @Translation("organizations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count organization",
 *     plural = "@count organizations",
 *   ),
 *   handlers = {
 *    "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *    "list_builder" = "Drupal\reyl_sc_organization\OrganizationListBuilder",
 *    "route_provider" = {
 *       "html" =
 *   "Drupal\reyl_sc_organization\Entity\OrganizationRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\reyl_sc_organization\Form\OrganizationForm",
 *       "edit" = "Drupal\reyl_sc_organization\Form\OrganizationForm"
 *     }
 *   },
 *   base_table = "organization",
 *   revision_table = "organization_revision",
 *   revision_data_table = "organization_field_revision",
 *   admin_permission = "administer organization entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "enabled" = "enabled",
 *     "collaborator_id" = "collaborator_id"
 *   },
 *   links = {
 *     "canonical" = "/organization/{organization}",
 *     "add-form" = "/organization/add",
 *     "edit-form" = "/admin/content/organization/{organization}/edit",
 *     "delete-form" = "/admin/content/organization/{organization}/delete",
 *     "version-history" =
 *   "/admin/content/organization/{organization}/revisions",
 *     "revision" =
 *   "/admin/content/organization/{organization}/revisions/{organization_revision}/view",
 *     "revision_revert" =
 *   "/admin/content/organization/{organization}/revisions/{organization_revision}/revert",
 *     "revision_delete" =
 *   "/admin/content/organization/{organization}/revisions/{organization_revision}/delete",
 *     "collection" = "/admin/structure/organization",
 *   },
 *   field_ui_base_route = "entity.organization.collection"
 * )
 */
class Organization extends RevisionableContentEntityBase implements OrganizationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'owner' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the organization owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * @return \Drupal\user\Entity\User
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($flag) {
    $this->set('enabled', $flag ? TRUE : FALSE);
    return $this;
  }


  /**
   * @return \Drupal\reyl_sc_collaborator\Entity\Collaborator[]
   */
  public function getCollaborators() {
    return $this->get('collaborator_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCollaboratorId() {
    return $this->get('collaborator_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setCollaboratorId($uid) {
    $this->set('collaborator_id', $uid);
    return $this;
  }

  /**
   * @param \Drupal\reyl_sc_collaborator\Entity\Collaborator[] $collaborators
   *
   * @return $this
   */
  public function setCollaborators(array $collaborators) {
    $ids = [];

    foreach ($collaborators as $collaborator) {
      $ids[]['target_id'] = $collaborator->id();
    }

    $this->set('collaborator_id', $ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The owner of the organization.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the organization.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['collaborator_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Collaborators'))
      ->setDescription(t('The collaborators of the organization.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'collaborator')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }


}
