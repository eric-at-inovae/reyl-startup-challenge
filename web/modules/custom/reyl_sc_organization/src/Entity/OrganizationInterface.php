<?php

namespace Drupal\reyl_sc_organization\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Organization entities.
 *
 * @ingroup reyl_sc_organization
 */
interface OrganizationInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Organization name.
   *
   * @return string
   *   Name of the Organization.
   */
  public function getName();

  /**
   * Sets the Organization name.
   *
   * @param string $name
   *   The Organization name.
   *
   * @return \Drupal\reyl_sc_organization\Entity\OrganizationInterface
   *   The called Organization entity.
   */
  public function setName($name);

  /**
   * Gets the Organization creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Organization.
   */
  public function getCreatedTime();

  /**
   * Sets the Organization creation timestamp.
   *
   * @param int $timestamp
   *   The Organization creation timestamp.
   *
   * @return \Drupal\reyl_sc_organization\Entity\OrganizationInterface
   *   The called Organization entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Organization enabled status indicator.
   *
   * @return bool
   *   TRUE if the Organization is enabled.
   */
  public function isEnabled();

  /**
   * Sets the enabled status of a Organization.
   *
   * @param bool $flag
   *   TRUE to set this Organization to enabled, FALSE to set it to disabled.
   *
   * @return \Drupal\reyl_sc_organization\Entity\OrganizationInterface
   *   The called Organization entity.
   */
  public function setEnabled($flag);

  /**
   * Gets the Organization revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Organization revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\reyl_sc_organization\Entity\OrganizationInterface
   *   The called Organization entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Organization revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Organization revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\reyl_sc_organization\Entity\OrganizationInterface
   *   The called Organization entity.
   */
  public function setRevisionUserId($uid);

}
