<?php

namespace Drupal\reyl_sc_organization\Plugin\Field\FieldItemList;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the organization name.
 */
class OrganizationNameItemList extends FieldItemList
{
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);

    // Make sure that subsequent getter calls do not try to compute the values
    // again.
    $this->valueComputed = TRUE;
  }

  /**
   * Computes the values for an item list.
   */
  protected function computeValue()
  {
    /** @var \Drupal\reyl_sc_organization\Entity\Organization $organization */
    $organization = $this->getEntity();

    $this->list[0] = $this->createItem(0, $organization->getName());

    $this->valueComputed = TRUE;
  }
}
