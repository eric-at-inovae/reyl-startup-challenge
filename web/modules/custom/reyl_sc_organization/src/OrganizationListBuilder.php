<?php

namespace Drupal\reyl_sc_organization;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Organization entities.
 *
 * @ingroup reyl_sc_organization
 */
class OrganizationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['owner_name'] = $this->t('Owner');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\reyl_sc_organization\Entity\Organization */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.organization.canonical',
      [
        'organization' => $entity->id()
      ]
    );
    $row['owner_name'] = Link::createFromRoute(
      $entity->getOwner()->getDisplayName(),
      'entity.user.canonical',
      [
        'user' => $entity->getOwnerId()
      ]
    );

    return $row + parent::buildRow($entity);
  }
}
