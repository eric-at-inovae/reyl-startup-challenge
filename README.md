# REYL Startup Challenge

## Prerequisites

* PHP 7.1

## Local installation

* Create a web server virtual host (either locally of using the Docker image of your preference) which document root points to the `web` folder of this project.

* Create an empty database using either MariaDB or MySQL (either locally of using the Docker image of your preference).

* Copy `web/sites/default/settings.local.example.php` to `web/sites/default/settings.local.php` and edit `web/sites/default/settings.local.php` with the database settings.

```bash
cp web/sites/default/settings.local.example.php web/sites/default/settings.local.php
```

* Install the site

```bash
composer site:install
```

* That's it! Open the website from your favorite browser and log as `administrator` using the `admin` password - or use Drupal console to get a one-time login.

## Execute tests

`composer test:unit`

## Convenience scripts

### Rebuild the cache

`composer cr`
